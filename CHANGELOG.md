# Changelog

## 1.0.1 - 2024-05-07

### Changed

- Compatible SPIP 4.y.z

## 1.0.0 - 2023-07-10

### Added

- Première sortie publique
