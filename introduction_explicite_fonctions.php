<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}
/**
 * Compile la balise `#INTRODUCTION_INTRODUCTION`
 *
 * Retourne une introduction d'un objet éditorial, c'est à dire le contenu
 * indiqué entre `<intro>` et `</intro>` du champ TEXTE.
 *
 * @param Champ $p
 *     Pile au niveau de la balise
 * @return Champ
 *     Pile complétée par le code à générer
 **/
function balise_INTRODUCTION_EXPLICITE_dist($p) {
	$type_objet = $p->type_requete;
	$cle_objet = id_table_objet($type_objet);
	$_id_objet = champ_sql($cle_objet, $p);

	// Récupérer les valeurs sql nécessaires : descriptif, texte et chapo
	// ainsi que le longueur d'introduction donnée dans la description de l'objet.
	$_ligne = 'array(';
	$trouver_table = charger_fonction('trouver_table', 'base');
	if ($desc = $trouver_table(table_objet_sql($type_objet))) {
		if (isset($desc['field']['descriptif'])) {
			$_ligne .= "'descriptif' => " . champ_sql('descriptif', $p) . ',';
		}
		if (isset($desc['field']['texte'])) {
			$_ligne .= "'texte' => " . champ_sql('texte', $p) . ',';
		}
		if (isset($desc['field']['chapo'])) {
			$_ligne .= "'chapo' => " . champ_sql('chapo', $p) . ',';
		}
	}
	$_ligne .= ')';


	$p->code = "generer_objet_introduction_explicite((int)$_id_objet, '$type_objet', $_ligne, \$connect)";

	#$p->interdire_scripts = true;
	$p->etoile = '*'; // propre est deja fait dans le calcul de l'intro
	return $p;
}


/**
 * Fonction privée pour donner l'introduction explicite (marqué par `<intro>`) d'un objet de manière générique.
 *
 * Cette fonction est mutualisée entre les balises #INTRODUCTION et #INFO_INTRODUCTION.
 * Elle se charge de faire le tri entre descriptif, texte et chapo,
 * Ensuite elle fait appel au filtre 'introduction' qui construit celle-ci à partir de ces données.
 *
 * @uses filtre_introduction_dist()
 * @see generer_info_entite()
 * @see balise_INTRODUCTION_dist()
 *
 * @param int $id_objet
 *     Numéro de l'objet
 * @param string $type_objet
 *     Type d'objet
 * @param array $ligne_sql
 *     Ligne SQL de l'objet avec au moins descriptif, texte et chapo
 * @param string $connect
 *     Nom du connecteur à la base de données
 * @return string
 */
function generer_objet_introduction_explicite(int $id_objet, string $type_objet, array $ligne_sql, string $connect = ''): string {

	$texte = $ligne_sql['descriptif'] ?? '';

	if (!$texte) {
		$texte = $ligne_sql['texte'] ?? '';
		// En absence de descriptif, on se rabat sur chapo + texte
		if (isset($ligne_sql['chapo'])) {
			$chapo =  $ligne_sql['chapo'];
			$texte = "$chapo \n\n $texte";
		}
	}

	preg_match_all('/<intro>(.*)<\/intro>/ms', $texte, $matches);
	$texte = $matches[1][0] ?? '';

	if (!$texte) {
		return $texte;
	}


	// ne pas tenir compte des notes
	if ($notes = charger_fonction('notes', 'inc', true)) {
		$notes('', 'empiler');
	}
	// Supprimer les modèles avant le propre afin d'éviter qu'ils n'ajoutent du texte indésirable
	// dans l'introduction.
	$introduction = supprime_img($texte, '');
	$introduction = appliquer_traitement_champ($texte, 'introduction', '', [], $connect);

	include_spip('inc/filtres');
	$introduction = supprimer_tags($introduction);
	$introduction = trim($introduction);

	if ($notes) {
		$notes('', 'depiler');
	}
	return $introduction;
}
